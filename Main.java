import java.util.Scanner;

public class Main {

	private static Scanner scan;

	public static void main(String[] args) {
		System.out.println("Viberete jelaemuyu figuru: 1 - krug, 2 - priamougolnik");
		scan = new Scanner(System.in);
		int vibor = scan.nextInt();
		if (vibor == 1) { // Zdes' mozhno vibrat' jelaemuyu figuru dlya nahojdeniya ee ploshadi
			System.out.println("Vvedite radius");
			int r = scan.nextInt();
			Figura Figur = new Krug(r); // Na etom etape sozdaetsya krug, kak objekt klassa figura
			System.out.println("Imya figuri : " + Figur.getImya());
			System.out.println("Ploshad' figuri : " + Figur.getPloshyad());
		}
		if (vibor == 2) {
			System.out.println("Vvedite visotu");
			int v = scan.nextInt();
			System.out.println("Vvedite shirinu");
			int sh = scan.nextInt();
			Figura Figur = new Pryamougolnik(v, sh); // Zdes' sozdaetsya pryamougol'nik, kak objekt klassa figura
			System.out.println("Imya figuri : " + Figur.getImya());
			System.out.println("Ploshad' figuri : " + Figur.getPloshyad());
		}
	}
}
