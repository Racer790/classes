
public class Krug extends Figura {
	private double radius;
	private double Pi;

	public Krug(double r) {
		radius = r;
		Pi = 3.1415;
	}

	@Override
	public String getImya() {
		return "Krug";
	}

	@Override
	public double getPloshyad() {
		return Pi * (radius * radius);

	}

}
